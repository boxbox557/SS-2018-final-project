/* main game state */

var rewardState = {
	/** variable declaration **/
	reward: 0,
	/* object */
	keyboard: null,
	
	/** phaser function **/
	create: function() {
		// set interface
		game.stage.backgroundColor = '#FFE1DD';
		var text = game.add.text(250, 100, 'Job success!', {font: '48px'});
		//text = game.add.text(game.width/2 - 150, 250, 'Furniture durability: ' + game.global.durability, {font: '36px'});
		this.reward = game.global.durability * 23;
		text = game.add.text(game.width/2 - 150, 250, 'Reward: $' + this.reward, {font: '36px'});
		game.global.money += this.reward;
		text = game.add.text(game.width/2 - 150, 350, 'Now you have: $' + game.global.money, {font: '36px'});
		text = game.add.text(game.width/2, 500, 'Press ENTER to continue', {font: '24px', fill: '#555555'});
		text.anchor.set(0.5, 0.5);
		
		// create keyboard object
		this.keyboard = game.input.keyboard.addKeys({
			'enter': Phaser.KeyCode.ENTER,
		});
		this.keyboard.enter.onDown.add(this.pressENTER, this);
	},
	
	/** customized function **/
	pressENTER: function (){
		// go to next level
		if(game.global.level+1 >= game.global.maxLevel){
			// 玩家成功通關
			game.state.start('menu');
		}else{
			game.global.level = game.global.level + 1;
			if(game.global.level > game.global.highestLevel)
				game.global.highestLevel = game.global.level;
			game.state.start('gearChoosing');
		}
		
	}
}