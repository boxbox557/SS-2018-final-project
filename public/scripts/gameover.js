/* main game state */

var gameoverState = {
	
	/** phaser function **/
	create: function() {
		// set interface
		game.stage.backgroundColor = '#FFE1DD';
		var text = game.add.text(250, 100, 'Job failed! :(', {font: '48px'});
		text = game.add.text(350, 250, 'Your customer is not happy about your service.' , {font: '36px'});
		// game.global.money += this.reward;
		//text = game.add.text(game.width/2 - 150, 350, 'Now you have: $', {font: '36px'});
		text = game.add.text(game.width/2, 500, 'Press ENTER to return to menu', {font: '24px', fill: '#555555'});
		text.anchor.set(0.5, 0.5);
		
		// create keyboard object
		this.keyboard = game.input.keyboard.addKeys({
			'enter': Phaser.KeyCode.ENTER,
		});
		this.keyboard.enter.onDown.add(this.pressENTER, this);
	},
	
	/** customized function **/
	pressENTER: function (){
			game.state.start('menu');
	}
}