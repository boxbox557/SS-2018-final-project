
var game = new Phaser.Game(1200, 600, Phaser.AUTO, 'canvas');

game.global = {
	/* declare phaser global variables here */
	userDevice: 'desktop',	// the device type user use. (desktop/phone)
	level: 0,				// current level of user
	highestLevel: 0,		// the highest level of user
	maxLevel: 3,			// number of total levels
	destinationPos: [],		// 終點座標
	durability: 100,		// 玩家通關時的家具耐久
	gearName: ['pokemon', 'mexicanhat', 'jaws', 'rocket'],
	gearArray:[],
	toolName: ['kit', 'pepper', 'tape', 'mrS', 'omega', 'dog', 'cloud', 'ufo'],
	toolArray:[],			//0,沒有; 1,有但未裝備 ; 2,裝備中
	surprise: [],			// 紀錄每關隨機道具出現位置
	backgroundMusic:null,
	money: 0				// 玩家當前金錢
}

/* phaser state */
game.state.add('prepare', prepareState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('level',levelState);
game.state.add('gearChoosing', gearChoosingState);
game.state.add('mainGame', mainGameState);
game.state.add('reward', rewardState);
game.state.add('gameover', gameoverState);

// start state
game.state.start('prepare');
