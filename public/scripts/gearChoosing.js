/* gearChoosing State */

var gearChoosingState = {
	/** variable declaration **/
	keyboard : null,			// 鍵盤元件集合 ()
	scrollingMap : null,
	currentPage : 0,
	pageText:null,
	page:0,
	buyingBlock:0,
	blockPic:null,
	toolCountText:[], //存道具數量文字label
	moneyPriceText:[],//顯示配件價格
	lock:[],
	check:[],
	gear:[],
	tool:[],
	title:[],
	text:[],
	selectBlock:[],
	bg:null,
	explain:null,
	gain:0,
	moneyIcon: null, //money圖標群組
	money:null,
	moneylist:[1000,1000,2500,5000,10,10,10,10], // 物件價格，拜託幫忙設一下
	/** phaser function **/
	create: function() {
		// set interface
		this.scrollingMap = game.add.tileSprite(0, 0 , 2400, 600, "gear");
		game.stage.backgroundColor = "#ffeedd"; 
		console.log("TOOL : ")
		console.log(game.global.toolArray);

		this.gain = 0;
		this.currentPage = 0;
		this.page = 0;

		this.pageText = game.add.text(game.width / 2, 16, "Tool Bag", {font: "30px Arial", fill: "#000044"})
		this.pageText2 = game.add.text(game.width / 2, 55, "prepare yourself before go to work!", {font: "20px Arial", fill: "#000044"})
		this.pageText.anchor.set(0.5,0.2);
		this.pageText2.anchor.set(0.5,0.2);
		
		this.bg = game.add.sprite(game.width*2/3-50,game.height/4,'bg');
		this.wallet = game.add.sprite(game.width/8,game.height/7,'wallet');
		this.wallet.visible = false;

		this.money = game.add.text(game.width/8+50,game.height/7+12,"$ "+game.global.money,{font: "20px Arial", fill: "#000044"})
		this.money.visible = false;
		this.moneyIcon = game.add.group();//money圖標群組
		this.explain = game.add.text(game.width/6,game.height*8/9,"press Z to change store page / SPACE to equip gear or select tool / ENTER to start game",{font: "20px Arial", fill: "#000000"})
		
		for(x=0;x<8;++x){
			game.global.toolArray[x].status = false;
			if(x<4){
				if(x==0) 
				{
					this.tool[x] = game.add.sprite(game.width*(x+1)/8, game.height/3,'kit');
					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Repair Kit",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"I better repair the furniture!",{font: "20px Arial", fill: "#000000"});
					
					this.gear[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'pokemon_icon');
					this.gear[x].visible = false;

					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Gotta Cath‘em All",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"Pikachu !!!",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 
				}
				if(x==1)
				{
					this.tool[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'pepper');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Carolina Reaper Pepper",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"Annie: “Well~ I guess I can accept \n15% more damage to the furniture.” ",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'mexicanhat_icon');
					this.gear[x].visible = false;
	
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Mexican Hat",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"¡Es excelente!",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 


				}
				if(x==2)
				{
					this.tool[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'tape');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"A bundle of tape",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"I prefer tape over ropes to steady \nthe furniture.",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'jaws_icon');
					this.gear[x].visible = false;
				
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Jaws",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"Have the mind of a predator. \nReduce truck destruction by 10% \nevery collision.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 

				} 
				if(x==3) 
				{
					this.tool[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'mrS');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Ms. S",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"Just in case I need to intimidate my \nclient into accepting the furniture as \nit is.",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'roket_icon');
					this.gear[x].visible = false;
	
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"To infinity and beyond!",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"Space delivery is provided.(Press \nup arrow to trigger, jumping will \nbe disabled.)",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 

				}
				
				this.toolCountText[x] = game.add.text(game.width*(x+1)/8+30, game.height/3+70, game.global.toolArray[x].number,{font: "15px Arial", fill: "#000000"}); //顯示道具數量
				this.moneyPriceText[x] = game.add.text(game.width*(x+1)/8+30, game.height/3+70, this.moneylist[x],{font: "15px Arial", fill: "#000000"});
				this.moneyPriceText[x].visible = false;
				var img = game.add.image(game.width*(x+1)/8+10, game.height/3+71, 'money_icon', 0, this.moneyIcon);
				img.scale = {x:0.5, y:0.5};

				this.lock[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'lock2');
				//this.lock[x].scale.setTo(0.04,0.04);
				if(game.global.toolArray[x].number>0) this.lock[x].visible = false;
				else this.lock[x].visible = true;
				this.selectBlock[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'selectblock');

				this.check[x] = game.add.sprite(game.width*(x+1)/8,game.height/3,'check');
				this.check[x].scale.setTo(0.04,0.04);
				this.check[x].visible = false;

				if(x!=0) this.selectBlock[x].visible = false;
				
				
			}
			else{
				if(x==4) 
				{
					this.tool[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'omega');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Spartan Rage",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"L3 + R3",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'potatocake');
					this.gear[x].visible = false;
	
					// this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"To infinity and beyond!",{font: "23px Arial", fill: "#ff0000"});
					// this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"Space delivery is provided.(Press \nup arrow to trigger, jumping will \nbe disabled.)",{font: "20px Arial", fill: "#000000"});
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Oops!",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"This potatocake ate your gear.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 

				}
				if(x==5) 
				{
					this.tool[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'dog');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Humans’ best friend",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"You’re going to help me? \nReally? Thanks!!",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'potatocake');
					this.gear[x].visible = false;
				
					// this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Dusty",{font: "23px Arial", fill: "#ff0000"});
					// this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"He will save you from a hard crash. \nWhen the truck is destroyed, \ngain 20% endurance.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Oops!",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"This potatocake ate your gear.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 
				}
				if(x==6) 
				{
					this.tool[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'cloud');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Every cloud has a silver lining.",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+80,"Are you sure this saying applies to \nevery cloud? Cause I’m sure some \nclouds are just pitch-black.",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'potatocake');
					this.gear[x].visible = false;
				
					// this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"A blessing from Roger Federer",{font: "23px Arial", fill: "#ff0000"});
					// this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"Invite Roger Federer to sit at \nthe back of the truck and get \nbonus money!",{font: "20px Arial", fill: "#000000"});
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Oops!",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"This potatocake ate your gear.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 
				}
				if(x==7) 
				{
					this.tool[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'ufo');

					this.title[x] = game.add.text(game.width*2/3-30,game.height/4+20,"Your mother is so fat that \neven aliens can’t abduct her.",{font: "23px Arial", fill: "#ff0000"});
					this.text[x] = game.add.text(game.width*2/3-30,game.height/4+110,"Log 8532: “We tried to lift it up, but \nwe couldn’t hold it for long.”",{font: "20px Arial", fill: "#000000"});
					this.title[x].visible = false;
					this.text[x].visible = false;

					this.gear[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'potatocake');
					this.gear[x].visible = false;
				
					// this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Angel Wings",{font: "23px Arial", fill: "#ff0000"});
					// this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"Glide gracefully toward your \ndestination.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8] = game.add.text(game.width*2/3-30,game.height/4+20,"Oops!",{font: "23px Arial", fill: "#ff0000"});
					this.text[x+8] = game.add.text(game.width*2/3-30,game.height/4+80,"This potatocake ate your gear.",{font: "20px Arial", fill: "#000000"});
					this.title[x+8].visible = false;
					this.text[x+8].visible = false; 

				}
				
				this.toolCountText[x] = game.add.text(game.width*(x-3)/8+30, game.height*2/3+70, game.global.toolArray[x].number,{font: "15px Arial", fill: "#000000"}); //顯示道具數量
				this.moneyPriceText[x] = game.add.text(game.width*(x-3)/8+30, game.height*2/3+70, this.moneylist[x],{font: "15px Arial", fill: "#000000"});
				this.moneyPriceText[x].visible = false;

				var img = game.add.image(game.width*(x-3)/8+10, game.height*2/3+71, 'money_icon', 0, this.moneyIcon);
				img.scale = {x:0.5, y:0.5};

				this.lock[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'lock2');
				//this.lock[x].scale.setTo(0.04,0.04)
				if(game.global.toolArray[x].number>0) this.lock[x].visible = false;
				else this.lock[x].visible = true;

				this.selectBlock[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'selectblock');
				this.selectBlock[x].visible = false;

				this.check[x] = game.add.sprite(game.width*(x-3)/8,game.height*2/3,'check');
				this.check[x].scale.setTo(0.04,0.04);
				this.check[x].visible = false;

			}
		}

		this.moneyIcon.visible = false; //money圖標群組不可見
		this.blockPic = game.add.sprite(game.width/3,game.height/2,'buyingBlock');
		this.blockPic.visible = false;
		
		// start next state
		this.keyboard = game.input.keyboard.addKeys({
			'up': Phaser.KeyCode.UP,
			'down': Phaser.KeyCode.DOWN,
			'z': Phaser.KeyCode.Z,
			'left': Phaser.KeyCode.LEFT,
			'right': Phaser.KeyCode.RIGHT,
			'space': Phaser.KeyCode.SPACEBAR,
			'esc': Phaser.KeyCode.ESC,
			'enter': Phaser.KeyCode.ENTER,
		});
		
        this.keyboard.left.onDown.add(this.pressLeft, this);
		this.keyboard.right.onDown.add(this.pressright, this);
		this.keyboard.up.onDown.add(this.pressUp,this);
		this.keyboard.down.onDown.add(this.pressDown,this);
		this.keyboard.space.onDown.add(this.pressSpace,this);
		this.keyboard.esc.onDown.add(this.pressEsc,this);
		this.keyboard.z.onDown.add(this.pressZ,this);
		this.keyboard.enter.onDown.add(this.pressEnter,this);
		//game.state.start('mainGame');
		console.log("GEAR!!");
	},
	update: function() {

		//updateToolnum();
	},
	/** customize function **/

	updateToolnum: function() { //更新道具數量顯示，未完成
		for(x=0;x<8;++x){
			this.toolCountText[x].text = game.global.toolArray[x].number;
		}
	},
	show:function(){
		for(a=0;a<8;a++)
		{
			if(this.page==0)
			{
				if(game.global.toolArray[a].number==0&& game.global.toolArray[a].status == false) 
				{
					this.lock[a].visible = true;
					this.check[a].visible = false;
				}
				else if(game.global.toolArray[a].number>0 && game.global.toolArray[a].status == false)
				{
					this.lock[a].visible = false;
					this.check[a].visible = false;
				}
				else if(game.global.toolArray[a].number>0 && game.global.toolArray[a].status == true){
					this.lock[a].visible = false;
					this.check[a].visible = true;
				}
			}

			else
			{
				if(game.global.gearArray[a]==0) 
				{
					this.lock[a].visible = true;
					this.check[a].visible = false;
				}
				else if(game.global.gearArray[a]==1)
				{
					this.lock[a].visible = false;
					this.check[a].visible = false;
				}
				else{
					this.lock[a].visible = false;
					this.check[a].visible = true;
				}

			}
		}
	},
	pressLeft:function(){
		if(this.buyingBlock==0){
			if(this.currentPage>0) this.changeSelect(-1);
			else this.changeSelect(0);	
		}

	},
	pressright:function(){
		if(this.buyingBlock==0){
			if(this.currentPage<7) this.changeSelect(1);
        	else this.changeSelect(0);
		} 
	},
	pressUp:function(){
		if(this.buyingBlock==0){
			if(this.currentPage>3) this.changeSelect(-4);
        	else this.changeSelect(0);
		} 
	},
	pressDown: function(){
		if(this.buyingBlock==0){
			if(this.currentPage<4) this.changeSelect(4);
        	else this.changeSelect(0);
		} 
	},
	pressSpace:function(){
		if(this.page==1)
		{
			if(this.buyingBlock==0){
				if(this.currentPage<4){
					if(game.global.gearArray[this.currentPage]==0&&game.global.money >= this.moneylist[this.currentPage])
					{
						this.blockPic.visible = true;
						this.buyingBlock = 1;	
					}
					else if(game.global.gearArray[this.currentPage]==1)
					{
						game.global.gearArray[this.currentPage] = 2;
						this.check[this.currentPage].visible = true;
					}
					else if(game.global.gearArray[this.currentPage]==2){
						game.global.gearArray[this.currentPage] = 1;
						this.check[this.currentPage].visible = false;
					}
				}
			}
			else{
				game.global.money -= this.moneylist[this.currentPage];
				this.money.text = "$ "+game.global.money;
				this.blockPic.visible = false;
				this.buyingBlock = 0;
				game.global.gearArray[this.currentPage] = 1;
				this.lock[this.currentPage].visible = false;
				console.log(game.global.gearArray[this.currentPage]);
			}
		}
		else 
		{
			this.buyingBlock.visible = false;
			if(game.global.toolArray[this.currentPage].number>0)
			{
				if(game.global.toolArray[this.currentPage].status==false && this.gain ==0)
				{
					game.global.toolArray[this.currentPage].status = true;
					this.check[this.currentPage].visible = true;
					this.gain = 1;
				}
				else if(game.global.toolArray[this.currentPage].status==true)
				{
					game.global.toolArray[this.currentPage].status=false;
					this.check[this.currentPage].visible = false;
					this.gain = 0;
				}
			}
		}
	},
	pressEsc: function(){
		console.log("PRESS ESC");
		if(this.buyingBlock==1){
			this.blockPic.visible = false;
			this.buyingBlock = 0;
		}
		else{
			//console.log("底特律萬歲!!!!");
			game.global.money = 10000;
			for(a=0;a<8;a++)
			{
				game.global.toolArray[a].number = 1;
			}
		}
	},
	pressZ:function(){
		this.changePage();
		this.show();
		
	},
	pressEnter:function(){
		game.state.start("mainGame");
	},
	changeSelect:function(page){
		this.currentPage += page;
		if(page==-1){
			this.selectBlock[this.currentPage+1].visible = false;
			this.selectBlock[this.currentPage].visible = true;
			this.title[8*this.page+this.currentPage+1].visible = false;
			this.text[8*this.page+this.currentPage+1].visible = false;
			this.title[8*this.page+this.currentPage].visible = true;
			this.text[8*this.page+this.currentPage].visible = true;
			

		}
		else if(page==1){
			this.selectBlock[this.currentPage-1].visible = false;
			this.selectBlock[this.currentPage].visible = true;
			this.title[8*this.page+this.currentPage-1].visible = false;
			this.text[8*this.page+this.currentPage-1].visible = false;
			this.title[8*this.page+this.currentPage].visible = true;
			this.text[8*this.page+this.currentPage].visible = true;

		}
		else if(page==-4){
			this.selectBlock[this.currentPage+4].visible = false;
			this.selectBlock[this.currentPage].visible = true;
			this.title[8*this.page+this.currentPage+4].visible = false;
			this.text[8*this.page+this.currentPage+4].visible = false;
			this.title[8*this.page+this.currentPage].visible = true;
			this.text[8*this.page+this.currentPage].visible = true;

		}
		else if(page==4){
			this.selectBlock[this.currentPage-4].visible = false;
			this.selectBlock[this.currentPage].visible = true;
			this.title[8*this.page+this.currentPage-4].visible = false;
			this.text[8*this.page+this.currentPage-4].visible = false;
			this.title[8*this.page+this.currentPage].visible = true;
			this.text[8*this.page+this.currentPage].visible = true;

		}
		else if(page==0){

		}

	},
	changePage: function(){
		if(this.page==0){
			this.pageText.text = "Gear Store"; 
			var tween = game.add.tween(this.scrollingMap).to({
				 x: 1200    
			}, 300, Phaser.Easing.Cubic.Out, true);
			this.page = 1;
			for(a=0;a<8;a++){
				this.tool[a].visible = false;
				this.gear[a].visible = true;
			}
			this.title[this.currentPage].visible = false;
			this.text[this.currentPage].visible = false;
			this.title[8+this.currentPage].visible = true;
			this.text[8+this.currentPage].visible = true;
			this.wallet.visible = true;
			this.money.visible = true;
			this.moneyIcon.visible = true; //money圖標群組不可見
			for(x=0;x<8;++x) {
				this.toolCountText[x].visible = false;
				this.moneyPriceText[x].visible = true;
			}
		}
		else{
			this.pageText.text = "Tool Bag"; 
			var tween = game.add.tween(this.scrollingMap).to({
				 x: 0    
			}, 300, Phaser.Easing.Cubic.Out, true);
			for(a=0;a<8;a++){
				this.tool[a].visible = true;
				this.gear[a].visible = false;
			}
			this.moneyIcon.visible = false; //money圖標群組不可見
			this.page = 0;
			this.title[this.currentPage].visible = true;
			this.text[this.currentPage].visible = true;
			this.title[8+this.currentPage].visible = false;
			this.text[8+this.currentPage].visible = false;
			this.wallet.visible = false;
			this.money.visible = false;
			for(x=0;x<8;++x) {
				this.toolCountText[x].visible = true;
				this.moneyPriceText[x].visible = false;
			}
		}
   }
	
};