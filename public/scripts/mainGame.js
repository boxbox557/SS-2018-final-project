/* main game state */

var mainGameState = {
	/** variable declaration **/
	/* fixed parameter */ 
	level: 0,				// 當前關卡
	destination: {},		// 終點座標
	reduction: 1,			// 耐久減免 (pepper/ jaw可增加減免)
	goal: 20,				// 該關目標
	maxCarSpeed: 300,		// 最高車速
	maxWheelPower: 50,		// 蓄力條最大值
	/* dynamic parameter */ 
	frontWheelPower: 0,		// 前輪蓄力條數值
	backWheelPower: 0,		// 後輪蓄力條數值
	durability: 100,		// 當前家具耐久
	rdmToolIdx: 0,			// 每關驚喜ID
	isToolPicked: false,	// 每關驚喜是否已撿取
	onDestination: false,	// 是否在終點上
	isSpace: null,			// 已經按過SPACE
	pepper: null,			// 使用辣椒
	tape: null,				// 使用膠帶
	mrS: 0,					// 使用Mr. S
	cloudRand: null,		// 決定左邊或右邊雲的亂數
	iscloudLeft: null,		// 左雲
	iscloudRight: null,		// 右雲
	isUFO: null,			// 使用幽浮
	isHighest: null,		// 判斷UFO是否最高y
	fridge_h: null, 		//紀錄最高時家具的高度
	lamp_h: null, 
	sofa_h: null, 
	tv_h: null, 
	table_h: null,
	/* objects */
	durabilityText: null,	// 顯示家具耐久
	toolBox: null,			// 該關道具
	toolBoxBlock: null, 	// 道具鎖
	goalText: null,			// 顯示該關目標
	player: null,			// 玩家物件
	house: null,			// 終點圖片
	furnitures: null,		// 家具group
	fridge: null,			// 家具們
	lamp: null,
	sofa: null,
	table: null,
	tv: null,
	truckMaterial: null,	// 卡車介面(摩擦力)
	furnitureMaterial: null,// 家具介面(摩擦力)
	contactMaterial: null,	// 接觸介面
	dogs: null,				// 校狗物件
	dogCurtain: null,		// 校狗黑幕
	flyufo: null,			// 幽浮物件
	rdmTool: null,			// 每關驚喜
	map: null,				// tilemap地圖
	layer: null,			// 關卡圖層
	keyboard: null,			// 鍵盤元件集合 (up/ down/ left/ right/ z/ x/ space/ esc)
	frontPowerBar: null,	// 前輪蓄力條
	backPowerBar: null,		// 後輪蓄力條
	powerBoostingTimer: null,	// 蓄力遞增計時器/0.05s
	powerDecadeTimer: null,		// 蓄力遞減計時器/0.1s
	endGameTimer: null,			// 終點計時器
	spartaRoar: null,			// Sound effect for Spartan Rage
	barking: null,				// Sound effect for dogs
	
	/** phaser function **/
	create: function() {
		// initialize parameters
		this.level = game.global.level;
		this.destination = game.global.destinationPos[this.level];
		this.isToolPicked = false;
		this.onDestination = false;
		this.reduction = 1;
		this.pepper = 0;
		this.tape = 0;
		this.mrS = 0;
		this.isSpace = 0;
		this.iscloudLeft = 0;
		this.iscloudRight = 0;
		this.isUFO = 0;
		this.isHighest = 0;
		this.durability = 100;
		game.physics.p2.gravity.y = 300;
		game.physics.p2.restitution = 0.3;

		// set interface
		game.stage.backgroundColor = '#FFE1DD';
		this.loadMap();
		// add durability
		this.durabilityText = game.add.text(1100, 65, this.durability, {font: '36px', fill: '#FF0000'});
		this.durabilityText.fixedToCamera = true;
		// add powerBar
		this.frontPowerBar = game.add.sprite(127, 200, 'powerBar');
		this.frontPowerBar.fixedToCamera = true;
		this.frontPowerBar.anchor.set(0, 1);
		this.frontWheelPower = 0;
		this.backPowerBar = game.add.sprite(77, 200, 'powerBar');
		this.backPowerBar.fixedToCamera = true;
		this.backPowerBar.anchor.set(0, 1);
		this.backWheelPower = 0;
		// add tool box
		var toolIdx = -1;
		// 找到該關所選擇的道具
		for(i=0; i<8; ++i){
			if(game.global.toolArray[i].status==true){
				toolIdx = i;
				break;
			}
		}
		if(toolIdx>=0){
			this.toolBox = game.add.image(1125, 125, game.global.toolName[i]);
			this.toolBox.scale = {x:0.75, y:0.75};
			this.toolBox.fixedToCamera = true;
		}
		this.toolBoxBlock = game.add.image(1125, 125, 'toolBlock');
		this.toolBoxBlock.scale = {x:0.75, y:0.75};
		this.toolBoxBlock.fixedToCamera = true;
		if(toolIdx>=0)
			this.toolBoxBlock.visible = false;

		var img = game.add.image(75, 50, 'powerFrame');
		img.fixedToCamera = true;
		img = game.add.image(125, 50, 'powerFrame');
		img.fixedToCamera = true;
		
		this.setPlayer();	// load player and furniture
		
		// add audio
		if(game.global.gearArray[1]==2){
			game.global.backgroundMusic.stop();
			game.global.backgroundMusic = game.add.audio('mexico_bgm');
			game.global.backgroundMusic.loop = true;	
			game.global.backgroundMusic.play();
		}
		this.spartaRoar = game.add.audio('sparta');
		this.barking = game.add.audio('bark');
		
		// get keyboard element
		this.keyboard = game.input.keyboard.addKeys({
			'up': Phaser.KeyCode.UP,
			'down': Phaser.KeyCode.DOWN,
			'left': Phaser.KeyCode.LEFT,
			'right': Phaser.KeyCode.RIGHT,
			'r': Phaser.KeyCode.R,
			'z': Phaser.KeyCode.Z,
			'x': Phaser.KeyCode.C,
			'space': Phaser.KeyCode.SPACEBAR,
			'esc': Phaser.KeyCode.ESC
		});
		this.keyboard.r.onDown.add(this.pressR, this);
		this.keyboard.up.onDown.add(this.pressUP, this);
		this.keyboard.esc.onDown.add(this.pressEsc, this);
		this.keyboard.space.onDown.add(this.pressSpace, this);
		
		// set timer
		this.powerBoostingTimer = game.time.events.loop(50, this.boostPower, this);
		this.powerDecadeTimer = game.time.events.loop(100, this.decadePower, this);
	},
	update: function() {
		
		this.playerDetect();
		this.keyboardDetect();
		this.playerAnimation();
		this.durabilityDetect();
		
		// update powerBar
		this.frontPowerBar.height = this.frontWheelPower * 3;
		this.backPowerBar.height = this.backWheelPower * 3;

		// use cloud tool
		if(this.iscloudLeft==1)
			this.player.body.velocity.x += 9;
		if(this.iscloudRight==1)
			this.player.body.velocity.x -= 9;

		// ufo make car fly
		if(this.isUFO==1){
			this.flyufo.x = this.player.body.x-60;
			this.player.body.data.gravityScale = 0;
			this.fridge.body.data.gravityScale = 0;
			this.lamp.body.data.gravityScale = 0;
			this.sofa.body.data.gravityScale = 0;
			this.table.body.data.gravityScale = 0;
			this.tv.body.data.gravityScale = 0;
			if(this.player.body.y<=200 && !this.isHighest){
				this.isHighest = 1;
				this.fridge_h = this.fridge.body.y;
				this.lamp_h = this.lamp.body.y;
				this.sofa_h = this.sofa.body.y;
				this.tv_h = this.tv.body.y;
				this.table_h = this.table.body.y;
			}
			if (this.isHighest){
				this.player.body.y = 200;
				this.fridge.body.y = this.fridge_h;
				this.lamp.body.y = this.lamp_h;
				this.sofa.body.y = this.sofa_h;
				this.tv.body.y = this.tv_h;
				this.table.body.y = this.table_h;
			}
			else{
				this.player.body.y -= 5;
				this.fridge.body.y -= 5;
				this.lamp.body.y -= 5;
				this.sofa.body.y -= 5;
				this.tv.body.y -= 5;
				this.table.body.y -= 5;
			}
		}
		else if(this.isUFO==2){	// ufo make car fall
			this.player.body.y += 5;
			this.fridge.body.y += 5;
			this.lamp.body.y += 5;
			this.sofa.body.y += 5;
			this.tv.body.y += 5;
			this.table.body.y += 5;
		}
	},
	
	/** customized function **/
	/* load map of corrent level (called at create function) */
	setPlayer: function() {
		// load car
		if(game.global.gearArray[0] == 2){
			// if equip pokemon
			this.player = game.add.sprite(350, 402.5, 'car_pokemon');
		}else{
			this.player = game.add.sprite(350, 402.5, 'car');
		}
		game.physics.p2.enable(this.player);
		this.player.anchor.set(0.5,0.5);
		this.player.animations.add('right', [0,1,2], 15, true);
		this.player.animations.add('left', [2,1,0], 15, true);
		this.player.fixedRotation = true;
		
		// set materials(friction between truck/furniture and table/tv)
		this.truckMaterial = game.physics.p2.createMaterial('truckMaterial');
		this.furnitureMaterial = game.physics.p2.createMaterial('furnitureMaterial');
		game.physics.p2.setWorldMaterial('truckMaterial',true,true,true,true);
		this.contactMaterial = game.physics.p2.createContactMaterial(this.truckMaterial, this.furnitureMaterial);
		this.contactMaterial.friction = 1.5;  // This is coefficient of friction of truck/furniture
		
		// add furniture
		this.furnitures = game.add.group();
		this.fridge = game.add.sprite(240, 393, 'fridge', 0, this.furnitures);
		this.lamp = game.add.sprite(280, 393, 'lamp', 0, this.furnitures);
		this.sofa = game.add.sprite(325, 413, 'sofa', 0, this.furnitures);
		this.table = game.add.sprite(380, 418, 'table', 0, this.furnitures);
		this.tv = game.add.sprite(382,375,'tv',0,this.furnitures);
		game.physics.p2.enable(this.furnitures);
		// clear default shape
		this.player.body.clearShapes();
		this.furnitures.callAll('body.clearShapes', 'body');			
		// add shapes
		this.player.body.loadPolygon('physicsData','car_spritesheet_gap');
		this.fridge.body.addRectangle(49, 87);
		this.fridge.body.mass = 0.3;
		this.lamp.body.loadPolygon('physicsData','lamp');
		this.sofa.body.loadPolygon('physicsData', 'sofa');
		this.table.body.loadPolygon('physicsData', 'table');
		this.tv.body.loadPolygon('physicsData','tv');
		// apply material
		this.player.body.setMaterial(this.truckMaterial);
		this.furnitures.callAll('body.setMaterial', 'body', this.furnitureMaterial);
		
		// when car touch anything, call the functioin 'bodyContact()'
		this.player.body.onBeginContact.add(this.bodyContact, this);
		game.camera.follow(this.player);
		
		// if equip the jaws, add the durability reduction
		// gear
		for(gearIdx=0; gearIdx<game.global.gearName.length; gearIdx++){
			if(game.global.gearArray[gearIdx]==2){
				switch(gearIdx){
				case 1:
					this.player.addChild(game.make.sprite(-5, -127, 'mexicanhat'));
					break;
				case 2:
					this.player.addChild(game.make.sprite(-135, -125, 'jaws'));
					break;
				case 3:
					this.rocket = this.player.addChild(game.make.sprite(-60, 45, 'roket_spritesheet'));
					this.rocket.animations.add('fly', [0, 1, 2, 3], 15, true);
					break;
				}
			}
		}
		if(game.global.gearArray[2]==2)
			this.reduction *= 0.9;
	},
	loadMap: function() {
		// Create map
		this.map = game.add.tilemap('level' + (this.level+1));
		this.map.addTilesetImage('tileset');			// Add the tileset to the map
		this.layer = this.map.createLayer('Layer1');	// Create level layer
		this.layer.resizeWorld();
		this.map.setCollision([1, 2]);					// Enable collision for both tileset elements
		var floor = game.physics.p2.convertTilemap(this.map, this.layer);
		
		// add surprise
		this.rdmToolIdx = Math.floor((Math.random() * game.global.toolName.length) + 1) - 1;	// pick tool randomly
		var surprisePosArr = game.global.surprise[this.level];
		var rdmPosIdx = Math.floor((Math.random() * surprisePosArr.length) + 1) - 1;	// pick position randomly
		this.rdmTool = game.add.sprite(surprisePosArr[rdmPosIdx].x, surprisePosArr[rdmPosIdx].y, game.global.toolName[this.rdmToolIdx]);
		// debug message
		console.log('Surprise tool id: ' + this.rdmToolIdx);
		console.log('Surprise position: ' + surprisePosArr[rdmPosIdx].x + ' ' + surprisePosArr[rdmPosIdx].y);
		// goal message
		this.goalText = game.add.text(1000, 30, 'Goal: durability >= ', {font:'15px Verdana',fill:'#FF0000'});
		this.goalText.fixedToCamera = true;
		
		switch(this.level + 1){
		case 1:
			// add texts for first level
			game.add.text(80,250,'Try not to destroy the furniture!\nPress → to move forward\nPress ← to move backward',{font:'30px Verdana',fill:'#000000'});
			game.add.text(680,250,'Press Z to store power in back wheel\nPress C to store power in front wheel\nthen press ↑ to jump!',{font:'30px Verdana',fill:'#000000'});
			game.add.text(1500,250,'Press ESC to return to menu\nPress SPACE to use your tool',{font:'30px Verdana',fill:'#000000'});
			game.add.text(2100,250,'Now try to get over this hill!!',{font:'30px Verdana',fill:'#000000'});
			game.add.text(2800,250,'You can collect tools on your way!\nYou can use the tool in the next level',{font:'30px Verdana',fill:'#000000'});
			game.add.text(3500,250,'Stay on the house for 3 secs\nto finish the job!',{font:'30px Verdana',fill:'#000000'});
			this.goal = 50;
			break;
		case 2:
			this.goal = 30;
			break;
		case 3:
			this.goal = 10;
			break;
		}
		// add destination picture
		this.house = game.add.sprite(this.destination.x+70, this.destination.y-112 ,'house');
		this.goalText.text = 'Goal: durability >= ' + this.goal;
		
	},
	// detect if car is overlapped with special item
	playerDetect: function() {
		// if touch tool
		if(Math.abs(this.player.x - this.rdmTool.x) < 120 && Math.abs(this.player.y - this.rdmTool.y) < 50){
			if(!this.isToolPicked){
				this.rdmTool.destroy();
				game.global.toolArray[this.rdmToolIdx].number++ ;
				this.isToolPicked = true;
			}
		}
		if(Math.abs(this.player.centerX - this.house.centerX) < 130 && Math.abs(this.player.centerY - this.house.centerY) < 60){
			if(!this.onDestination){
				this.onDestination = true;
				this.endGameTimer = game.time.events.add(3000, this.endGame, this);
			}
		}else{
			if(this.onDestination){
				game.time.events.remove(this.endGameTimer);
			}
			this.onDestination = false;
		}
	},
	/** the function that will be called when car contact anything 
	 * body: the item car is contact with
	 * bodyB: where particle of car does it contact body
	 * shapeA: body's shape
	 * shapeB: bodyB's shape
	 * equation
	 */
	bodyContact: function(body, bodyB, shapeA, shapeB, equation) {
		var speedDifferenceX, speedDifferenceY;
		if(body){
			if(body.sprite){
				// if sprite exist => touch furniture
				speedDifferenceX = Math.abs(this.player.body.velocity.x - body.sprite.body.velocity.x);
				speedDifferenceY = Math.abs(this.player.body.velocity.y - body.sprite.body.velocity.y);
			}else{
				// if sprite doesn't exist => touch tilemap
				if(this.isUFO==2){
					// if ufo is making car fall and car touched tilemap
					this.isUFO = 0;
					this.player.body.data.gravityScale = 1;
					this.fridge.body.data.gravityScale = 1;
					this.lamp.body.data.gravityScale = 1;
					this.sofa.body.data.gravityScale = 1;
					this.table.body.data.gravityScale = 1;
					this.tv.body.data.gravityScale = 1;
					this.flyufo.visible = false;
				}else{
					// else, damage the durability
					speedDifferenceX = Math.abs(this.player.body.velocity.x);
					speedDifferenceY = Math.abs(this.player.body.velocity.y);
				}
			}
		}
		else{
			// if body doesn't exist => touch world bound
			speedDifferenceX = Math.abs(this.player.body.velocity.x);
			speedDifferenceY = Math.abs(this.player.body.velocity.y);
		}
		
		// calculate the damage of durability
		var damage = speedDifferenceX*0.8 + speedDifferenceY*0.75;
		if(damage >= 50){
			this.durability -= (damage/500)*this.reduction;
			this.durabilityText.text = this.durability.toFixed(1);
		}
	},
	/* check and boost wheel power every 0.05 sec */
	boostPower: function() {
		if(this.keyboard.x.isDown && this.frontWheelPower < this.maxWheelPower){
			this.frontWheelPower += 1;
		}
		if(this.keyboard.z.isDown && this.backWheelPower < this.maxWheelPower){
			this.backWheelPower += 1;
		}
	},
	/* decade wheel power every 0.1 sec */
	decadePower: function() {
		if(!this.keyboard.x.isDown){
			this.frontWheelPower = Math.max(this.frontWheelPower-1, 0);
		}
		if(!this.keyboard.z.isDown){
			this.backWheelPower = Math.max(this.backWheelPower-1, 0);
		}
	},
	/* keyboard detect function */
	keyboardDetect: function() {
		// movement detect
		if(this.keyboard.left.isDown){
			if(this.player.body.velocity.x > (0-this.maxCarSpeed))
				this.player.body.velocity.x -= 15;
		}else if(this.keyboard.right.isDown){
			if(this.player.body.velocity.x < this.maxCarSpeed)
				this.player.body.velocity.x += 15;
		}else {
			// 減速
			if(this.player.body.velocity.x > 0)
				this.player.body.velocity.x  = Math.max(0, this.player.body.velocity.x - 5);
			if(this.player.body.velocity.x < 0)
				this.player.body.velocity.x  = Math.min(0, this.player.body.velocity.x + 5);
		}
		
		if(game.global.gearArray[3]==2){
			if(this.keyboard.up.isDown){
				// rocket fly
				this.player.body.velocity.y -= 100;
				this.rocket.animations.play('fly');
			}else{
				this.rocket.frame = 4;
			}
		}
	},
	/* play animation */
	playerAnimation: function() {
		if(this.player.body.velocity.x > 0)
			this.player.animations.play('right');
		else if(this.player.body.velocity.x < 0)
			this.player.animations.play('left');
		else
			this.player.frame = 0;
	},
	durabilityDetect: function() {
		if(this.mrS == 0){
			if(this.pepper == 0){
				if(this.durability < this.goal){
					this.endMusic();
					game.state.start('gameover');
				}
			}
			else if(this.pepper == 1){
				if(this.durability < this.goal-20){
					this.endMusic();
					game.state.start('gameover');
				}
			}
		}
	},
	pressUP: function() {
		if(game.global.gearArray[3]!=2){
			// jump normally
			/** 施加瞬間作用力
			 * applyImpulseLocal(施加向量[x, y], 相對x座標, 相對y座標)
			 */
			this.player.body.applyImpulseLocal([0, this.frontWheelPower*1.2], 30, 0);
			this.player.body.applyImpulseLocal([0, this.backWheelPower*1.2], -50, 0);
		}
	},
	pressEsc: function() {
		// back to menu
		this.endMusic();
		game.state.start('menu');
	},
	pressSpace: function() {
		if(this.isSpace==0){
			this.toolBoxBlock.visible = true;
			if(game.global.toolArray[0].status==true){	// repair kit
				this.durability += 20;
				if(this.durability > 100)
					this.durability = 100;
				this.durabilityText.text = this.durability.toFixed(1);
				game.global.toolArray[0].status = false;
				game.global.toolArray[0].number--;
			}
			else if(game.global.toolArray[1].status==true){	// pepper
				this.pepper = 1;
				this.reduction *= 0.5
				this.goalText.text = 'Goal: durability >= 5';
				game.global.toolArray[1].status = false;
				game.global.toolArray[1].number--;
			}
			else if(game.global.toolArray[2].status==true){	// tape
				this.tape = 1;
				game.global.toolArray[2].status = false;
				game.global.toolArray[2].number--;
			}else if(game.global.toolArray[3].status==true){	// Mr. S
				this.mrS = 1;
				game.global.toolArray[3].status = false;
				game.global.toolArray[3].number--;
			}
			else if(game.global.toolArray[4].status==true){ // Spartan Rage
				this.spartaRoar.play();
				this.player.body.applyImpulseLocal([0, 80], -50, 0);
				game.global.toolArray[4].status = false;
				game.global.toolArray[4].number--;
			}
			else if(game.global.toolArray[5].status==true){	// dog
				this.barking.play();
				this.dogs = this.player.addChild(game.make.sprite(-200,10,'dogs'));
				game.time.events.add(1000,this.blackCurtain,this);
				game.global.toolArray[5].status = false;
				game.global.toolArray[5].number--;
			}
			else if(game.global.toolArray[6].status==true){	// cloud
				this.cloudRand = game.rnd.integerInRange(0,50);
				if(this.cloudRand<25){
					this.player.addChild(game.make.sprite(-330,-250,'cloudLeft'));
					this.iscloudLeft = 1;
				}
				else{
					this.player.addChild(game.make.sprite(140,-250,'cloudRight'));
					this.iscloudRight = 1;
				}
				game.global.toolArray[6].status = false;
				game.global.toolArray[6].number--;
			}
			else if(game.global.toolArray[7].status==true){	// UFO
				this.flyufo = game.add.sprite(this.player.body.x-60,20,'realUFO');
				this.isUFO = 1;
				game.time.events.add(5000,this.fallUFO,this);
				game.global.toolArray[7].status = false;
				game.global.toolArray[7].number--;
			}else{
				//this.toolBoxBlock.visible = false;
			}
		}
		this.isSpace = 1;
	},
	fallUFO: function() {
		this.isUFO = 2;
	},
	blackCurtain: function() {
		this.dogCurtain = game.add.sprite(0,0,'curtain');
		this.dogCurtain.fixedToCamera = true;
		this.dogCurtain.alpha = 0;
		game.add.tween(this.dogCurtain).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true);
		game.time.events.add(2000,this.curtainOut,this);
	},
	curtainOut: function() {
		this.player.body.x = this.destination.x+200;
		this.player.body.y = this.destination.y-67.5;
		this.fridge.body.x = this.player.body.x-110;
		this.fridge.body.y = this.player.body.y-9.5;
		this.lamp.body.x = this.player.body.x-70;
		this.lamp.body.y = this.player.body.y-9.5;
		this.sofa.body.x = this.player.body.x-25;
		this.sofa.body.y = this.player.body.y+10.5;
		this.table.body.x = this.player.body.x+30;
		this.table.body.y = this.player.body.y+15.5;
		this.tv.body.x = this.player.body.x+32;
		this.tv.body.y = this.player.body.y-27.5;
		game.add.tween(this.dogCurtain).to( { alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
		this.dogs.visible = false;
	},
	pressR: function() {
		game.state.start('mainGame');
	},
	endMusic: function() {
		if(game.global.gearArray[1]==2){
			// 將BGM改回來
			game.global.backgroundMusic.stop();
			game.global.backgroundMusic = game.add.audio('music');
			game.global.backgroundMusic.loop = true;
			game.global.backgroundMusic.play();
		}
	},
	endGame: function() {
		if(this.onDestination){
			// 到獎勵頁面
			this.endMusic();
			game.global.durability = this.durability.toFixed(0);
			game.state.start('reward');
		}
	}
};// var playState
