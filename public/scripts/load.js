/* load State */

var loadState = {
	/** variable declaration **/
	loadBar:null,
	loadingLabel:null,
	/** phaser function **/
	preload: function() {
		// set loading material
		this.loadingLabel = game.add.text(game.width/3,230,'Loading...',{font:'80px Arial',fill:'#ffffff'});
		this.loadingLabel.anchor.setTo(0.5,0.5);
		// loading bar
		this.loadBar =  game.add.sprite(game.width/2,250,'loadingBar');
		this.loadBar.anchor.setTo(0.5,0.5);
		game.load.setPreloadSprite(this.loadBar,0);
		
		/* preload menu material */
		game.load.image("gameName","img/gameName.png");
		
		/* preload gear material */
		// interface
		game.load.image("bg","img/bg.png");
		game.load.image("gear", "img/gear.png");
		game.load.image("buyingBlock","img/buyingBlock.png");
		game.load.image("check","img/check.png");
		game.load.image("selectblock","img/selectblock.png");
		game.load.image('wallet', 'img/wallet.png');
		game.load.image('money_icon', 'img/money_icon.png');
		// tool
        game.load.image("cloud","img/tools/cloud.png");
        game.load.image("pepper","img/tools/pepper.png");
        game.load.image("kit","img/tools/repairkit.png");
        game.load.image("tape","img/tools/tape.png");
		game.load.image("ufo","img/tools/ufo.png");
		game.load.image("omega","img/tools/omega.png");
		game.load.image("dog","img/tools/dog.png");
		game.load.image("mrS","img/tools/mrS.png");
		game.load.image('realUFO','img/tools/ufo2.png');
		// gear
		game.load.image('jaws_icon', 'img/gears/jaws_icon.png');
		game.load.image('mexicanhat_icon', 'img/gears/mexicanhat_icon.png');
		game.load.image('pokemon_icon', 'img/gears/pokemon_icon.png');
		game.load.image('roket_icon', 'img/gears/roket_icon.png');
		game.load.image('jaws', 'img/gears/jaws.png');
		game.load.image('mexicanhat', 'img/gears/mexicanhat.png');
		game.load.image('potatocake', 'img/gears/potatocake.png');
		game.load.spritesheet('roket_spritesheet', 'img/gears/roket_spritesheet.png', 72, 101);
		
		/* preload gaming material */
		// interface
		game.load.image('powerFrame', 'img/powerFrame.png');
		game.load.image('powerBar', 'img/powerBar.png');
		game.load.image('toolBlock', 'img/tools/used.png');
		// sprite
		game.load.spritesheet('car', 'img/car_spritesheet_all.png',285,135);
		game.load.spritesheet('car_pokemon', 'img/car_spritesheet_pokemon.png',285,135);
		game.load.image('roof', 'img/car_roof.png');
		game.load.image('fridge', 'img/firdge.png');
		game.load.image('lamp', 'img/lamp.png');
		game.load.image('sofa', 'img/sofa.png');
		game.load.image('table', 'img/table.png');
		game.load.image('tv', 'img/tv.png');
		game.load.image('house','img/goal.png');
		game.load.image('cloudLeft', 'img/tools/cloudleft.png');
		game.load.image('cloudRight', 'img/tools/cloudright.png');
		game.load.image('dogs', 'img/tools/dog2.png');
		game.load.image('curtain', 'img/black.png');
		// data
		game.load.physics('physicsData', 'img/physicsEditor.json');
		// audio
		game.load.audio('music', ['music/backgroundMusic.ogg','music/backgroundMusic.mp3']);
		game.load.audio('mexico_bgm', ['music/mexico.ogg','music/mexico.mp3']);
		game.load.audio('sparta', ['music/sparta.ogg', 'music/sparta.mp3']);
		game.load.audio('bark', ['music/bark.ogg', 'music/bark.mp3']);
		// map
		game.load.image('tileset', 'map/tileset.png');
		game.load.tilemap('level1', 'map/level1.json', null, Phaser.Tilemap.TILED_JSON);
		game.load.tilemap('level2', 'map/level2.json', null, Phaser.Tilemap.TILED_JSON);
		game.load.tilemap('level3', 'map/level3.json', null, Phaser.Tilemap.TILED_JSON);
		
	},
	create: function() {
		// set interface
		game.stage.backgroundColor = '#5555FF';
		
		// initialize global variables
		game.global.money = 0;
		game.global.level = 0;
		game.global.highestLevel = 0;
		game.global.maxLevel = 3;
		game.global.destinationPos = [
			{x: 3580, y: 520},
			{x: 200, y: 1020},
			{x: 2860, y: 420}
		];
		game.global.surprise[0] = [
			{x: 1200, y: 475},
			{x: 2000, y: 500},
			{x: 2700, y: 475},
			{x: 2850, y: 450},
			{x: 3300, y: 430}
		];
		game.global.surprise[1] = [
			{x: 1040, y: 225},
			{x: 2600, y: 225},
			{x: 2850, y: 225},
			{x: 3700, y: 500},
			{x: 3560, y: 750},
			{x: 2500, y: 1050},
			{x: 1000, y: 850},
			{x: 1250, y: 850},
			{x: 300, y: 1110},
			{x: 1250, y: 1000},
			{x: 875, y: 1110}
		];
		game.global.surprise[2] = [
			{x: 760, y: 470}, //800, 500
			{x: 200, y: 400}, //200, 450 就在出生位置，太容易拿到了
			{x: 3800, y: 450},
			{x: 2920, y: 350}, //2920, 420
			{x: 2850, y: 100}//2850, 550
		];
		this.initArray();
		
		// enable physical engine
		game.physics.startSystem(Phaser.Physics.P2JS);
		// start next state
		game.state.start('menu');
		
	},
	update: function() {
		
	},
	initArray:function(){
		for(i=0;i<10;i++){
			game.global.gearArray[i] = 0;
			game.global.toolArray[i] = {
				status: false,
				number: 0
			}
		}
	}

	
	/** customize function **/
	
};