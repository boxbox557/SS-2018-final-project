/* prepare State */
var prepareState = {
	/** variable declaration **/
	
	/** phaser function **/
	preload: function() {
		/* preload loading state material */
		game.load.image('loadingBar', 'img/loadingBar.png');
	},
	create: function() {
		// start next state
		game.state.start('load');
	},
	
	/** customize function **/
	/**
	 * This is an example of function comment.
	 * parameter1: The explanation of parameter11
	 * parameter2: The explanation of parameter12
	 * returnValue: The explanation of return value
	 */
	exampleFunction1: function(parameter1, parameter2) {
		var returnValue = 0;
		return returnValue;
	},
	/**
	 * This is another example of function comment.
	 * This function will calculate the summation of inputs
	 * x: should be an integer
	 * y: should be an integer
	 * return: sum of x and y
	 */
	exampleFunction2: function(x, y) {
		return x + y;
	}
	
};