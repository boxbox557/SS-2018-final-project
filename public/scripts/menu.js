/* menu State */

var menuState = {
	/** variable declaration **/
	keyboard: null,			// 鍵盤元件集合 ()
	gameName: null,
	startText:null,
	car:null,
	preload:function(){
		game.load.image("lock", "img/lock.png");
		game.load.image("lock2", "img/tools/lock_black.png");
        game.load.image("transp", "img/testing.png");
	},
	/** phaser function **/
	create: function() {
		// set interface
		game.stage.backgroundColor = "#ffe1dd"; 
		//music play
		//console.log(game.global.backgroundMusic.isPlaying);
		
		if(game.global.backgroundMusic==null) 
		{	
			game.global.backgroundMusic = game.add.audio('music');
			game.global.backgroundMusic.loop = true;	
			game.global.backgroundMusic.play();
		}

		//game title
		this.gameName = game.add.sprite(game.width/6,0,'gameName');
		//car image
		this.car = game.add.sprite(game.width/3+60,game.height/2, 'car');
		this.car.animations.add('right', [0,1,2], 15, true);
		//start text
		this.startText = game.add.text(game.width/3+15,game.height*4/5, "Press ENTER to start", {font: "30px Courier", fill: "#000044"})
		// start next state
		this.keyboard = game.input.keyboard.addKeys({
			'enter': Phaser.KeyCode.ENTER,
		});
		this.keyboard.enter.onDown.add(this.pressEnter,this);
	},
	update: function() {
		this.car.animations.play('right');
	},
	/** customize function **/
	pressEnter:function(){
		game.state.start("level");
	}
};