var levelState = {

    levelCount:4,
    keyboard: null,
    currentPage:0,
    lock:null,

    preload: function(){
        
    },
   create: function(){  
        game.stage.backgroundColor = "#ffe1dd"; 
        
        this.currentPage = 0;
        this.pageText = game.add.text(game.width / 2, 16, "Swipe to select level page (1 / " + this.levelCount + ")", {font: "18px Arial", fill: "#000000"})
        this.pageText.anchor.set(0.5);
        
        this.scrollingMap = game.add.tileSprite(0,100 , 4800, 450, "transp");
		
		var text = game.add.text(game.width/2,500,"press ENTER to select level",{font: "36px", fill: "#FFFFFF"})
		text.anchor.set(0.5);

        this.lock =  game.add.sprite(game.width/4+40,game.height/2,'lock');
        //this.lock.scale.setTo(0.04,0.04);    
		

        this.keyboard = game.input.keyboard.addKeys({
			'left': Phaser.KeyCode.LEFT,
			'right': Phaser.KeyCode.RIGHT,
			'enter': Phaser.KeyCode.ENTER,
			'esc': Phaser.KeyCode.ESC
        });
        this.keyboard.left.onDown.add(this.pressLeft, this);
        this.keyboard.right.onDown.add(this.pressRight, this);
        this.keyboard.enter.onDown.add(this.pressEnter,this);
        
    },
    update: function(){
        this.show();
    },
    show:function(){
        if(this.currentPage > game.global.highestLevel)
        {
            this.lock.visible= true;
        } 
        else
        {
            this.lock.visible= false;
        }

    },
    pressLeft: function(){
        if(this.currentPage>0) this.changePage(-1);
        else this.changePage(0);
        console.log("LEFT");
    },
    pressRight: function(){
        if(this.currentPage+1 < this.levelCount) this.changePage(1);
        else this.changePage(0);
        console.log("RIGHT");
    },
    pressEnter:function(){
        
        if( this.currentPage <= game.global.highestLevel ){
			game.global.level = this.currentPage;
			game.state.start('gearChoosing');
		}
    },
    changePage: function(page){
        this.currentPage += page;
        this.pageText.text = "Swipe to select level page (" + (this.currentPage + 1).toString() + " / " + this.levelCount + ")"; 
        var tween = game.add.tween(this.scrollingMap).to({
             x: this.currentPage * - game.width    
        }, 300, Phaser.Easing.Cubic.Out, true);
        console.log(this.currentPage+"   "+game.global.highestLevel);

   }
};

