# SS-2018-finalProject 

## 黑車宅急便 
 **其他連結**
  * [雲端資料夾][google drive] (文件集中地)
  * [時程與分工表][time table]

 **注意事項**
  * 使用phaser CE 2.10.5，API使用可看[說明文件][phaser document]。
  * 若Console噴"Source Map Error"是正常的，不用管他。
  * 開始工作前記得先pull一次資料夾，以免要push的時候產生conflict。
  * commit message建議填寫。
  * **code務必加上註解，不然所有bug自己de，懂？**

 **Coding Style**
  * global變數需先在game.js宣告
  * state內部結構：
    > state自用變數宣告(需帶註解)
    > 3 phaser function(preload/create/update)
    > customize function(建議帶註解)
  * function前註解可加上function用途與參數說明，可參考prepare.js下方範例
  * 變數宣告方式同js慣例

[google drive]: https://drive.google.com/drive/folders/1_ZZoyhAkRV4XxsktbDSa649IVGJihs-J?usp=sharing
[time table]: https://docs.google.com/document/d/165au64XIpNRrjVJkdT2IuAzvlkpjEOxlij_bBibqUME/edit?usp=sharing
[phaser document]: https://photonstorm.github.io/phaser-ce/
